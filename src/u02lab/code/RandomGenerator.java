package u02lab.code;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

/**
 * Step 2
 *
 * Forget about class u02lab.code.RangeGenerator. Using TDD approach (create small test, create code that pass test, refactor
 * to excellence) implement the below class that represents a sequence of n random bits (0 or 1). Recall
 * that math.random() gives a double in [0,1]..
 * Be sure to test all that is needed, as before
 *
 * When you are done:
 * A) try to refactor the code according to DRY (u02lab.code.RandomGenerator vs u02lab.code.RangeGenerator)?
 * - be sure tests still pass
 * - refactor the test code as well
 * B) create an abstract factory for these two classes, and implement it
 */
public class RandomGenerator implements SequenceGenerator {

    private List<Integer> sequenceBit;
    private List<Integer> currentSequenceBit;
    private final int sequenceSize;
    private int currentIndex;


    public RandomGenerator(final int n){
        this.sequenceBit = new ArrayList<>();
        for(int i=0; i<= n; i++){
            int bit;
            if(Math.random()%2 == 0){
                bit=0;
            }else{
                bit=1;
            }
            this.sequenceBit.add(bit);
        }
        this.currentSequenceBit = this.sequenceBit;
        this.currentIndex = 0;
        this.sequenceSize = n;
    }

    @Override
    public Optional<Integer> next() {
        this.currentIndex++;
        return (this.currentIndex==this.sequenceSize) ? Optional.empty() :
                            Optional.of(this.currentSequenceBit.remove(this.currentIndex));
    }

    @Override
    public void reset() {
        this.currentSequenceBit = this.sequenceBit;
        this.currentIndex = -1;
    }

    @Override
    public boolean isOver() {
        if(this.currentSequenceBit.isEmpty()){
            return true;
        }
        return false;
    }

    @Override
    public List<Integer> allRemaining() {

        return this.currentSequenceBit;
    }
}
