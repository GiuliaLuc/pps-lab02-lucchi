package u02lab.code;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;

/**
 * Created by lucch on 07/03/2017.
 */
public class RangeGeneratorTest {



    private static final int START = 3;
    private static final int STOP = 5;

    private RangeGenerator rangeGenerator;

    @Before
    public void init(){
        this.rangeGenerator = new RangeGenerator(START, STOP);

    }

    private void toTheEnd(int start, int stop){
        for (int i=start; i<=stop;i++){
            rangeGenerator.next();
        }
        assertEquals(Optional.empty(), rangeGenerator.next());
    }

    @Test
    public void nextNumber() throws Exception{
        for(int i=START; i<=STOP;i++){
            assertEquals(Optional.of(i), rangeGenerator.next());
        }

    }
    
    @Test
    public void nextAfterStop() throws Exception{
        this.toTheEnd(START, STOP);
        assertEquals(Optional.empty(), rangeGenerator.next());

    }

    @Test
    public void resetAfterNext() throws Exception{
        this.rangeGenerator.next();
        this.rangeGenerator.next();
        this.rangeGenerator.reset();
        assertEquals(Optional.of(3), this.rangeGenerator.next());
    }

    @Test
    public void isOver() throws Exception {
        assertFalse(this.rangeGenerator.isOver());
        this.rangeGenerator.next();
        assertFalse(this.rangeGenerator.isOver());
        this.toTheEnd(START+1,STOP);
        assertTrue(this.rangeGenerator.isOver());

    }

    @Test
    public void givesAllRemainingNumbers() throws Exception {
        List<Integer> remaningNumbers = new ArrayList<>();
        this.rangeGenerator.next();
        for(int i=START+1; i<=STOP;i++){
            remaningNumbers.add(i);
        }
        assertEquals(remaningNumbers,this.rangeGenerator.allRemaining());

    }
}