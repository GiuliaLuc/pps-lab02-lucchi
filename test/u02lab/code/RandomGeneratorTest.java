package u02lab.code;

import org.hamcrest.core.AnyOf;
import org.junit.Before;
import org.junit.Test;

import java.util.Optional;

import static org.junit.Assert.*;

/**
 * Created by lucch on 07/03/2017.
 */
public class RandomGeneratorTest {


    private static final int SEQUENCE_SIZE = 10;
    private RandomGenerator randomGenerator;

    @Before
    public void init(){
        this.randomGenerator= new RandomGenerator(SEQUENCE_SIZE);
    }

    @Test
    public void isBinaryNumber() throws Exception {
        Optional<Integer> nextBit=this.randomGenerator.next();
        assertTrue(nextBit.equals(Optional.of(1)) || nextBit.equals(Optional.of(0)));
    }

    @Test
    public void isOver() throws Exception {
        this.randomGenerator.next();
        assertFalse(this.randomGenerator.isOver());
        Optional<Integer> bit = this.randomGenerator.next();
        if(bit.equals(Optional.empty())) {
            assertTrue(this.randomGenerator.isOver());
        }

    }


}